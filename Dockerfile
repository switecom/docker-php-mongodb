FROM php:5.6

RUN apt-get update && apt-get install -y unzip git libssl-dev && rm -rf /var/lib/apt/lists/*

RUN pecl install mongodb xdebug \
    && docker-php-ext-enable mongodb xdebug